FROM node:10.16.0

COPY ../testapp /testapp/

WORKDIR /testapp



RUN npm install
RUN $(npm bin)/ng build

FROM nginx

COPY --from=builder /testapp/dist/* /usr/share/nginx/html/

EXPOSE 80